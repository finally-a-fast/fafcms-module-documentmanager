[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module Documentmanager
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Changed byStatus query filter @cmoeke
- Unify save method @cmoeke
- Column sizes for fomantic @cmoeke
- Changed menu items @cmoeke

### Fixed
- Bug by using wrong classname for pdf renderer @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- Migration for font attributes @StefanBrandenburger
- Migration for database cleanup @StefanBrandenburger
- Search models @StefanBrandenburger
- Abstract parent class for document rendering @StefanBrandenburger
- Methods for (PDF) document rendering @StefanBrandenburger
- Action to get active documentversion-file + Url rule @StefanBrandenburger

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- Added new attributes in Documenttemplate.php (font_files, default_font) @StefanBrandenburger
- Added font generation and default font when geerating pdf in Pdf.php  @StefanBrandenburger
- PHP version in composer.json to ">=7.4" @StefanBrandenburger
- Renamed renderer/Pdf.php to renderer/PdfRenderer.php @StefanBrandenburger
- Code cleanup @StefanBrandenburger
- Changed document id to hashId @cmoeke
- Used runAction to use default get file action of file manager @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46
- Missing attributes in Documenttemplate.php (css_file, css_content) @StefanBrandenburger
- Missing attributes in model classes @StefanBrandenburger

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-documentmanager/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-documentmanager/-/tree/v0.1.0-alpha
