<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\renderer;

use fafcms\documentmanager\models\Document;
use fafcms\documentmanager\models\Documenttemplate;
use fafcms\documentmanager\models\Documentversion;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filegroup;
use fafcms\filemanager\models\Filetype;
use fafcms\parser\component\Parser;
use Yii;
use yii\base\ErrorException;
use yii\base\Model;
use yii\db\StaleObjectException;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

/**
 * Class DocumentRenderer
 *
 * @package fafcms\documentmanager\renderer
 */
abstract class DocumentRenderer extends Model
{
    public const PARSER_TYPE = Parser::TYPE_PAGE;

    /** @var object|false */
    public $content;

    /** @var string|null */
    public ?string $fileName = null;

    /** @var array */
    public array $meta = [];

    /** @var int */
    public int $filegroup_id;

    /** @var array of Filetype model  */
    public array $filetype;

    /** @var Document */
    public Document $document;

    /**
     * @param Documenttemplate|int $document
     * @param array                $meta
     *
     * @return object|false
     */
    abstract public static function renderDocument($document, $meta = []);

    /**
     * @return void
     */
    abstract protected function setFileType(): void;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();
        $this->setFileType();
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['content', 'document', 'filegroup_id', 'filetype'], 'required'],
            [['content'], 'validateContent'],
        ];
    }

    /**
     * @return bool
     */
    public function validateContent(): bool
    {
        return $this->content !== false;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     * @throws ErrorException
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        $this->content = static::renderDocument($this->document->documenttemplate, $this->meta);

        if ($runValidation && !$this->validate()) {
            return false;
        }

        $target = Filegroup::getPath($this->filegroup_id);

        if (!is_writable($target)) {
            throw new ErrorException('Directory is not writable. Please check the file permissions!');
        }

        $version = ($this->document->latestDocumentversion->version ?? 0) + 1;

        if ($this->fileName === null) {
            $this->fileName = Inflector::slug(Yii::$app->fafcmsParser->parse(Parser::TYPE_PDF, $this->document->documenttemplate->name . '-' . $version, Parser::ROOT, $this->meta));
        }

        $target    .= '/' . $this->fileName . '.' . $this->filetype['default_extension'];
        $fileModel  = $this->createFile($target);

        $documentversion = new Documentversion([
            'document_id' => $this->document->getPrimaryKey(),
            'version'     => $version,
            'file_id'     => $fileModel->getPrimaryKey(),
        ]);

        $documenttemplate       = $this->document->documenttemplate;
        $this->document->number = Yii::$app->fafcmsParser->parse(
            static::PARSER_TYPE,
            $documenttemplate->number_format,
            Parser::ROOT,
            ['number' => $documenttemplate->number_current + $documenttemplate->number_step_size] + $this->meta,
        );

        $documentversion->document = $this->document;

        if (!$documentversion->save()) {
            FileHelper::unlink($target);
            $fileModel->delete();
            $this->addError('document', $documentversion->getErrors());
            return false;
        }

        $this->document = $documentversion->document;

        return true;
    }

    /**
     * @param string   $target
     *
     * @return File
     * @throws ErrorException
     */
    protected function createFile(string $target): File
    {
        $fileModel = new File([
            'filename'     => $this->fileName,
            'filetype_id'  => $this->filetype['id'],
            'filegroup_id' => $this->filegroup_id,
            'size'         => filesize($target),
        ]);

        // needed to trigger SluggableBehavior to make sure that the filename is unique
        $fileModel->validate(['filename']);

        if (!$fileModel->save()) {
            FileHelper::unlink($target);
            throw new ErrorException('Cannot save file ' . print_r($fileModel->getErrors(), true));
        }

        return $fileModel;
    }
}
