<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\renderer;


use fafcms\filemanager\models\{File, Filetype};
use fafcms\documentmanager\models\{Documenttemplate, Documenttemplateelement};
use fafcms\parser\component\Parser;
use ScssPhp\ScssPhp\Compiler;
use Spipu\Html2Pdf\{Exception\Html2PdfException, Html2Pdf};
use Yii;
use yii\helpers\{ArrayHelper, Html};
use yii\base\ErrorException;

class PdfRenderer extends DocumentRenderer
{
    /** @var Html2Pdf|false */
    public $content;

    /**
     * @param Documenttemplate|int $document
     * @param array $meta
     *
     * @return false|Html2Pdf
     * @throws \Exception
     */
    public static function renderDocument($document, $meta = [])
    {
        if ($document instanceof Documenttemplate ) {
            $documenttemplate = $document;
        } elseif (is_int($document)) {
            $documenttemplate = Documenttemplate::findOne($document);
        }

        if (!isset($documenttemplate) || $documenttemplate === null) {
            return false;
        }

        try {
            $pdf = new Html2Pdf(($documenttemplate->orientation === 'landscape'?'L':'P'), [$documenttemplate->height * 10, $documenttemplate->width * 10], 'de', true, 'UTF-8', [
                $documenttemplate->margin_left * 10,
                $documenttemplate->margin_top * 10,
                $documenttemplate->margin_right * 10,
                $documenttemplate->margin_bottom * 10
            ]);

            $pdf->pdf->SetCreator(Yii::$app->name);
            $pdf->pdf->SetAuthor(Yii::$app->name);
            //$pdf->pdf->SetTitle($documenttemplate->name);
            //$pdf->pdf->setPrintHeader(false);
            //$pdf->pdf->setPrintFooter(false);

            $pdf->pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            $pdf->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // setSignature($signing_cert='', $private_key='', $private_key_password='', $extracerts='', $cert_type=2, $info=array(), $approval='') {
            // to create self-signed signature: openssl req -x509 -nodes -days 365000 -newkey rsa:1024 -keyout tcpdf.crt -out tcpdf.crt
            // to export crt to p12: openssl pkcs12 -export -in tcpdf.crt -out tcpdf.p12
            // to convert pfx certificate to pem: openssl
            //     OpenSSL> pkcs12 -in <cert.pfx> -out <cert.crt> -nodes
            //$pdf->SetAutoPageBreak(true, );

            $pdfContent = '';
            $documentTemplateElements = ArrayHelper::index($documenttemplate->getDocumenttemplateelements()->byStatus(Documenttemplateelement::STATUS_ACTIVE)->all(), null, 'type');

            $backTop = '0mm';
            $headerCount = count($documentTemplateElements['header']??[]);

            if ($headerCount > 0) {
                if ($headerCount > 1) {
                    throw new \Exception('Only one header element is allowed!');
                }

                $pdfContent .= '<page_header>' . self::renderElements($documentTemplateElements, $documenttemplate, $meta, 'header') . '</page_header>';
                $backTop = $documentTemplateElements['header'][0]->getSetting('backTop', $backTop);
            }

            $backBottom = '0mm';
            $footerCount = count($documentTemplateElements['footer'] ?? []);

            if ($footerCount > 0) {
                if ($footerCount > 1) {
                    throw new \Exception('Only one footer element is allowed!');
                }

                $pdfContent .= '<page_footer>' . self::renderElements($documentTemplateElements, $documenttemplate, $meta, 'footer') . '</page_footer>';
                $backBottom = $documentTemplateElements['footer'][0]->getSetting('backBottom', $backBottom);
            }

            $pdfContent .= self::renderElements($documentTemplateElements, $documenttemplate, $meta, 'content');

            $css = 'h1 {font-size:' . (PDF_FONT_SIZE_MAIN * 4.2) . 'pt;}';
            $css .= 'h2 {font-size:' . (PDF_FONT_SIZE_MAIN * 3.56) . 'pt;}';
            $css .= 'h3 {font-size:' . (PDF_FONT_SIZE_MAIN * 2.92) . 'pt;}';
            $css .= 'h4 {font-size:' . (PDF_FONT_SIZE_MAIN * 2.28) . 'pt;}';
            $css .= 'h5 {font-size:' . (PDF_FONT_SIZE_MAIN * 1.64) . 'pt;}';
            $css .= 'h6 {font-size:' . (PDF_FONT_SIZE_MAIN) . 'pt;}';

            if (!empty($documenttemplate->css_file)) {
                $cssFile = Yii::getAlias($documenttemplate->css_file);

                if (file_exists($cssFile)) {
                    $cssFileContent = file_get_contents($cssFile);
                    $pathInfo = pathinfo($cssFile);
                    $extension = $pathInfo['extension'];

                    if ($extension === 'sass' || $extension === 'scss') {
                        $scss = new Compiler();
                        $scss->setImportPaths([$pathInfo['dirname'], FAFCMS_BASE_PATH]);
                        $cssFileContent = $scss->compile($cssFileContent);
                    }

                    $css .= $cssFileContent;
                }
            }

            if (!empty($documenttemplate->css_content)) {
                $cssContent = $documenttemplate->css_content;

                if ($documenttemplate->css_type === 'sass' || $documenttemplate->css_type === 'scss') {
                    $scss = new Compiler();
                    $scss->setImportPaths([$pathInfo['dirname'], FAFCMS_BASE_PATH]);
                    $cssContent = $scss->compile($cssContent);
                }

                $css .= $cssContent;
            }

            if (!empty($documenttemplate->font_files)) {
                $fontFiles = $documenttemplate->getFontFiles();

                foreach ($fontFiles as $fontFile) {
                    if (file_exists(Yii::getAlias($fontFile))) {
                        \TCPDF_FONTS::addTTFfont(Yii::getAlias($fontFile));
                    }
                }
            }

            if (!empty($documenttemplate->default_font)) {
                $pdf->setDefaultFont($documenttemplate->default_font);
            }

            $pdfHtml = '<style type="text/css"><!--' . $css . ' --></style>' . '<page backtop="' . $backTop . '" backbottom="' . $backBottom . '" backleft="0" backright="0">' . $pdfContent . '</page>';

            $pdf->setTestTdInOnePage(true);
            $pdf->WriteHTML($pdfHtml);
        } catch (\Exception $e) {
            $pdf->clean();

            throw $e;
        }

        return $pdf;
    }

    /**
     * @param        $documentTemplateElements
     * @param        $documenttemplate
     * @param        $meta
     * @param string $type
     *
     * @return string
     */
    protected static function renderElements($documentTemplateElements, $documenttemplate, $meta, $type = 'content'): string
    {
        $pdfContent = '';

        if (count($documentTemplateElements[$type] ?? []) > 0) {
            foreach ($documentTemplateElements[$type] as $documenttemplateelement) {
                $options = [
                    'text-align' => $documenttemplateelement->align,
                    'float' => $documenttemplateelement->float,
                    'clear' => 'both',
                    'margin' => ($documenttemplateelement->margin_top * 10) . 'mm ' . ($documenttemplateelement->margin_right * 10) . 'mm ' . ($documenttemplateelement->margin_bottom * 10) . 'mm ' . ($documenttemplateelement->margin_left * 10) . 'mm',
                    'page-break-after' => 'avoid',
                    'page-break-before' => 'avoid'
                ];

                $tagName = 'span';

                if (!empty($documenttemplateelement->height)) {
                    $tagName = 'div';
                    $options['height'] = ($documenttemplateelement->height * 10) . 'mm';
                }

                if (!empty($documenttemplateelement->width)) {
                    $tagName = 'div';
                    $options['width'] = ($documenttemplateelement->width * 10) . 'mm';
                } else {
                    $options['width'] = '100%';
                }

                if ($documenttemplateelement->position === 'absolute') {
                    $options['position'] = 'absolute';
                    $tagName = 'div';
                    $options[$documenttemplateelement->oriantation_y] = ($documenttemplateelement->y * 10) . 'mm';
                    $options[$documenttemplateelement->oriantation_x] = ($documenttemplateelement->x * 10) . 'mm';
                }

                $content = Yii::$app->fafcmsParser->parse(Parser::TYPE_PDF, $documenttemplateelement->content, Parser::ROOT, $meta);

                $pdfContent .= Html::tag($tagName, $content, [
                    'style' => $options,$tagName
                ]);
            }
        }

        return $pdfContent;
    }

    /**
     * {@inheritdoc}
     * @throws ErrorException
     */
    protected function setFiletype(): void
    {
        if (($this->filetype = Filetype::find()->where(['mime_type' => 'application/pdf'])->asArray()->one()) === null) {
            throw new ErrorException('Cannot find filetype for mime type "application/pdf"');
        }
    }

    /**
     * {@inheritdoc}
     * @throws ErrorException|Html2PdfException
     */
    protected function createFile(string $target): File
    {
        $this->content->Output($target, 'F');
        return parent::createFile($target);
    }
}
