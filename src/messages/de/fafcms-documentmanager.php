<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

return [
    'Document Management' => 'Dokumentenverwaltung',
    'Document templates'  => 'Dokumentenvorlagen',
    'Document template'   => 'Dokumentenvorlage',
    'Documents'           => 'Dokumente',
    'Document'            => 'Dokument',
    'Document types'      => 'Dokumententypen',
    'Document type'       => 'Dokumententyp',
];
