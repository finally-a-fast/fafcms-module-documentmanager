<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use fafcms\helpers\ActiveRecord;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Json;

/**
 * This is the model class for table "documenttemplateelement".
 *
 * @property int $id
 * @property int $documenttemplatetranslation_id
 * @property int $source_documenttemplateelement_id
 * @property string $name
 * @property string $content
 * @property string $type
 * @property bool $is_visible
 * @property int $sort
 * @property double $width
 * @property double $height
 * @property string $float
 * @property string $align
 * @property string $valign
 * @property string $position
 * @property double $x
 * @property double $y
 * @property string $oriantation_x
 * @property string $oriantation_y
 * @property double $margin_top
 * @property double $margin_right
 * @property double $margin_bottom
 * @property double $margin_left
 * @property string $settings
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Documenttemplateelement $sourceDocumenttemplateelement
 * @property Documenttemplateelement[] $documenttemplateelements
 */
class Documenttemplateelement extends ActiveRecord
{
    /**
     * @todo use BeautifulModelTrait
     * @return array
     */
    public function getEditData()
    {
        return [
            'url' => 'documentmanager/element',
            'icon' => 'file-document',
            'plural' => Yii::t('app', 'Elements'),
            'singular' => Yii::t('app', 'Element')
        ];
    }

    private $_decodedSettings = null;

    /**
     * @param $name
     * @param $default
     *
     * @return mixed
     */
    public function getSetting($name, $default)
    {
        if ($this->_decodedSettings === null) {
            $this->_decodedSettings = Json::decode($this->settings);
        }

        return $this->_decodedSettings[$name] ?? $default;
    }

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%documenttemplateelement}}';
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes): void
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            $documenttemplateelements = Documenttemplateelement::find()->where([
                'source_documenttemplateelement_id' => $this->id
            ])->asArray()->all();

            $sourceLanguage = $this;
            unset($sourceLanguage['id']);
            unset($sourceLanguage['documenttemplatetranslation_id']);
            unset($sourceLanguage['source_documenttemplateelement_id']);
            unset($sourceLanguage['created_at']);
            unset($sourceLanguage['updated_at']);
            unset($sourceLanguage['created_by']);
            unset($sourceLanguage['updated_by']);
            unset($sourceLanguage['created_by_simulated']);
            unset($sourceLanguage['updated_by_simulated']);

            foreach($documenttemplateelements as $documenttemplateelement) {
                //TODO pürfen welche daten geänder wurden und nur diese überschreiben
               // $documenttemplateelement->setAttributes($sourceLanguage);
                //$documenttemplateelement->save();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['name', 'documenttemplate_id'], 'required'],
            [['remarks', 'content', 'settings'], 'string'],
            [['documenttemplate_id', 'sort', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['height', 'width', 'x', 'y', 'margin_top', 'margin_right', 'margin_bottom', 'margin_left'], 'number'],
            [['created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['status', 'name', 'type', 'float', 'align', 'valign', 'position', 'orientation_x', 'orientation_y'], 'string', 'max' => 255],
            [['documenttemplate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documenttemplate::class, 'targetAttribute' => ['documenttemplate_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id'                  => Yii::t('fafcms-documentmanager', 'ID'),
            'status'              => Yii::t('fafcms-documentmanager', 'Status'),
            'name'                => Yii::t('fafcms-documentmanager', 'Name'),
            'remarks'             => Yii::t('fafcms-documentmanager', 'Remarks'),
            'documenttemplate_id' => Yii::t('fafcms-documentmanager', 'Documenttemplate ID'),
            'content'             => Yii::t('fafcms-documentmanager', 'Content'),
            'type'                => Yii::t('fafcms-documentmanager', 'Type'),
            'sort'                => Yii::t('fafcms-documentmanager', 'Sort'),
            'height'              => Yii::t('fafcms-documentmanager', 'Height'),
            'width'               => Yii::t('fafcms-documentmanager', 'Width'),
            'float'               => Yii::t('fafcms-documentmanager', 'Float'),
            'align'               => Yii::t('fafcms-documentmanager', 'Align'),
            'valign'              => Yii::t('fafcms-documentmanager', 'Valign'),
            'position'            => Yii::t('fafcms-documentmanager', 'Position'),
            'x'                   => Yii::t('fafcms-documentmanager', 'X'),
            'y'                   => Yii::t('fafcms-documentmanager', 'Y'),
            'orientation_x'       => Yii::t('fafcms-documentmanager', 'Orientation X'),
            'orientation_y'       => Yii::t('fafcms-documentmanager', 'Orientation Y'),
            'margin_top'          => Yii::t('fafcms-documentmanager', 'Margin Top'),
            'margin_right'        => Yii::t('fafcms-documentmanager', 'Margin Right'),
            'margin_bottom'       => Yii::t('fafcms-documentmanager', 'Margin Bottom'),
            'margin_left'         => Yii::t('fafcms-documentmanager', 'Margin Left'),
            'settings'            => Yii::t('fafcms-documentmanager', 'Settings'),
            'created_by'          => Yii::t('fafcms-documentmanager', 'Created By'),
            'updated_by'          => Yii::t('fafcms-documentmanager', 'Updated By'),
            'activated_by'        => Yii::t('fafcms-documentmanager', 'Activated By'),
            'deactivated_by'      => Yii::t('fafcms-documentmanager', 'Deactivated By'),
            'deleted_by'          => Yii::t('fafcms-documentmanager', 'Deleted By'),
            'created_at'          => Yii::t('fafcms-documentmanager', 'Created At'),
            'updated_at'          => Yii::t('fafcms-documentmanager', 'Updated At'),
            'activated_at'        => Yii::t('fafcms-documentmanager', 'Activated At'),
            'deactivated_at'      => Yii::t('fafcms-documentmanager', 'Deactivated At'),
            'deleted_at'          => Yii::t('fafcms-documentmanager', 'Deleted At'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getSourceDocumenttemplateelement(): ActiveQuery
    {
        return $this->hasOne(static::class, ['id' => 'source_documenttemplateelement_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocumenttemplateelements(): ActiveQuery
    {
        return $this->hasMany(static::class, ['source_documenttemplateelement_id' => 'id']);
    }
}
