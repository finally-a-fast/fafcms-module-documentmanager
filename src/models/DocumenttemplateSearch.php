<?php
/**
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * DocumenttemplateSearch represents the model behind the search form of `Documenttemplate`.
 */
class DocumenttemplateSearch extends Documenttemplate
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'documenttype_id', 'number_current', 'number_step_size', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'name', 'remarks', 'number_format', 'orientation', 'css_file', 'css_content', 'css_type', 'font_files', 'default_font', 'data', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['height', 'width', 'margin_top', 'margin_right', 'margin_bottom', 'margin_left'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Documenttemplate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'               => $this->id,
            'documenttype_id'  => $this->documenttype_id,
            'number_current'   => $this->number_current,
            'number_step_size' => $this->number_step_size,
            'height'           => $this->height,
            'width'            => $this->width,
            'margin_top'       => $this->margin_top,
            'margin_right'     => $this->margin_right,
            'margin_bottom'    => $this->margin_bottom,
            'margin_left'      => $this->margin_left,
            'created_by'       => $this->created_by,
            'updated_by'       => $this->updated_by,
            'activated_by'     => $this->activated_by,
            'deactivated_by'   => $this->deactivated_by,
            'deleted_by'       => $this->deleted_by,
            'created_at'       => $this->created_at,
            'updated_at'       => $this->updated_at,
            'activated_at'     => $this->activated_at,
            'deactivated_at'   => $this->deactivated_at,
            'deleted_at'       => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'number_format', $this->number_format])
            ->andFilterWhere(['like', 'orientation', $this->orientation])
            ->andFilterWhere(['like', 'css_file', $this->css_file])
            ->andFilterWhere(['like', 'css_content', $this->css_content])
            ->andFilterWhere(['like', 'css_type', $this->css_type])
            ->andFilterWhere(['like', 'font_files', $this->font_files])
            ->andFilterWhere(['like', 'default_font', $this->default_font])
            ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
