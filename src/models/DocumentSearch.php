<?php
/**
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * DocumentSearch represents the model behind the search form of `Document`.
 */
class DocumentSearch extends Document
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'documenttemplate_id', 'active_version', 'active_documentversion_id', 'active_file_id', 'model_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'number', 'model_class', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Document::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                         => $this->id,
            'documenttemplate_id'        => $this->documenttemplate_id,
            'active_version'             => $this->active_version,
            'active_documentversion_id'  => $this->active_documentversion_id,
            'active_file_id'             => $this->active_file_id,
            'model_id'                   => $this->model_id,
            'created_by'                 => $this->created_by,
            'updated_by'                 => $this->updated_by,
            'activated_by'               => $this->activated_by,
            'deactivated_by'             => $this->deactivated_by,
            'deleted_by'                 => $this->deleted_by,
            'created_at'                 => $this->created_at,
            'updated_at'                 => $this->updated_at,
            'activated_at'               => $this->activated_at,
            'deactivated_at'             => $this->deactivated_at,
            'deleted_at'                 => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'model_class', $this->model_class]);

        return $dataProvider;
    }
}
