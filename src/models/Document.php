<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use fafcms\documentmanager\Bootstrap;
use fafcms\filemanager\models\File;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\OptionTrait;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string $status
 * @property int $documenttemplate_id
 * @property string $number
 * @property int $active_version
 * @property int $active_documentversion_id
 * @property int $active_file_id
 * @property string $model_class
 * @property int $model_id
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Documentversion $activeDocumentversion
 * @property Documentversion $latestDocumentversion
 * @property Documenttemplate $documenttemplate
 * @property Documentversion[] $documentversions
 */
class Document extends ActiveRecord
{
    use BeautifulModelTrait;
    use OptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/document';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'file-document-outline';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-documentmanager', 'Documents');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-documentmanager', 'Document');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['number'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            self::class,
            array_merge([
                self::tableName().'.id',
                self::tableName().'.number'
            ], $select ?? []),
            'id',
            static function ($item) {
                return self::extendedLabel($item);
            },
            null,
            $sort ?? [self::tableName() . '.number' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%document}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['documenttemplate_id', 'number', 'active_version', 'active_documentversion_id', 'active_file_id'], 'required'],
            [['documenttemplate_id', 'active_version', 'active_documentversion_id', 'active_file_id', 'model_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['status', 'number', 'model_class'], 'string', 'max' => 255],
            [['active_documentversion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documentversion::class, 'targetAttribute' => ['active_documentversion_id' => 'id']],
            [['active_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['active_file_id' => 'id']],
            [['documenttemplate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documenttemplate::class, 'targetAttribute' => ['documenttemplate_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id'                         => Yii::t('fafcms-documentmanager', 'ID'),
            'status'                     => Yii::t('fafcms-documentmanager', 'Status'),
            'documenttemplate_id'        => Yii::t('fafcms-documentmanager', 'Documenttemplate ID'),
            'number'                     => Yii::t('fafcms-documentmanager', 'Number'),
            'active_version'             => Yii::t('fafcms-documentmanager', 'Active Version'),
            'active_documentversion_id'  => Yii::t('fafcms-documentmanager', 'Active Documentversion ID'),
            'active_file_id'             => Yii::t('fafcms-documentmanager', 'Active File ID'),
            'model_class'                => Yii::t('fafcms-documentmanager', 'Model Class'),
            'model_id'                   => Yii::t('fafcms-documentmanager', 'Model ID'),
            'created_by'                 => Yii::t('fafcms-documentmanager', 'Created By'),
            'updated_by'                 => Yii::t('fafcms-documentmanager', 'Updated By'),
            'activated_by'               => Yii::t('fafcms-documentmanager', 'Activated By'),
            'deactivated_by'             => Yii::t('fafcms-documentmanager', 'Deactivated By'),
            'deleted_by'                 => Yii::t('fafcms-documentmanager', 'Deleted By'),
            'created_at'                 => Yii::t('fafcms-documentmanager', 'Created At'),
            'updated_at'                 => Yii::t('fafcms-documentmanager', 'Updated At'),
            'activated_at'               => Yii::t('fafcms-documentmanager', 'Activated At'),
            'deactivated_at'             => Yii::t('fafcms-documentmanager', 'Deactivated At'),
            'deleted_at'                 => Yii::t('fafcms-documentmanager', 'Deleted At'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getActiveDocumentversion(): ActiveQuery
    {
        return $this->hasOne(Documentversion::class, ['id' => 'active_documentversion_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLatestDocumentversion(): ActiveQuery
    {
        return $this->hasOne(Documentversion::class, ['document_id' => 'id'])->orderBy(['version' => SORT_DESC]);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocumenttemplate(): ActiveQuery
    {
        return $this->hasOne(Documenttemplate::class, ['id' => 'documenttemplate_id'])->inverseOf('documents');
    }

    /**
     * @return ActiveQuery
     */
    public function getDocumentversions(): ActiveQuery
    {
        return $this->hasMany(Documentversion::class, ['document_id' => 'id'])->inverseOf('document');
    }

    /**
     * @return ActiveQuery
     */
    public function getFile(): ActiveQuery
    {
        return $this->hasOne(File::class, ['id' => 'active_file_id'])->inverseOf('file');
    }
}
