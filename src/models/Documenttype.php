<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use fafcms\documentmanager\Bootstrap;
use fafcms\fafcms\inputs\AceEditor;
use fafcms\fafcms\inputs\DateTimePicker;
use fafcms\fafcms\inputs\DropDownList;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\OptionTrait;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "documenttype".
 *
 * @property int $id
 * @property string $name
 * @property int $documentgroup_id
 * @property string $remarks
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Documenttemplate[] $documenttemplates
 */
class Documenttype extends ActiveRecord implements FieldConfigInterface, EditViewInterface, IndexViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;

    public static string $searchModelClass = LayoutSearch::class;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/documenttype';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'file-cabinet';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-documentmanager', 'Document types');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-documentmanager', 'Document type');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            self::class,
            array_merge([
                self::tableName().'.id',
                self::tableName().'.name'
            ], $select ?? []),
            'id',
            static function ($item) {
                return self::extendedLabel($item);
            },
            null,
            $sort ?? [self::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => TextInput::class,
                'options' => [
                    'disabled' => true
                ]
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status']
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'content' => [
                'type' => AceEditor::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                [
                    'class' => \fafcms\fafcms\items\Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        's' => 8,
                                    ],
                                    'contents' => [
                                        [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        's' => 8,
                                    ],
                                    'contents' => [
                                        [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'file-eye-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Content'],
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'content',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'link' => true,
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    /**
     * @inheritdoc
     */
    public static function prefixableTableName(): string
    {
        return '{{%documenttype}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['name'], 'required'],
            [['remarks'], 'string'],
            [['created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['status', 'name'], 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id'             => Yii::t('fafcms-documentmanager', 'ID'),
            'status'         => Yii::t('fafcms-documentmanager', 'Status'),
            'name'           => Yii::t('fafcms-documentmanager', 'Name'),
            'remarks'        => Yii::t('fafcms-documentmanager', 'Remarks'),
            'created_by'     => Yii::t('fafcms-documentmanager', 'Created By'),
            'updated_by'     => Yii::t('fafcms-documentmanager', 'Updated By'),
            'activated_by'   => Yii::t('fafcms-documentmanager', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-documentmanager', 'Deactivated By'),
            'deleted_by'     => Yii::t('fafcms-documentmanager', 'Deleted By'),
            'created_at'     => Yii::t('fafcms-documentmanager', 'Created At'),
            'updated_at'     => Yii::t('fafcms-documentmanager', 'Updated At'),
            'activated_at'   => Yii::t('fafcms-documentmanager', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-documentmanager', 'Deactivated At'),
            'deleted_at'     => Yii::t('fafcms-documentmanager', 'Deleted At'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocumenttemplates(): ActiveQuery
    {
        return $this->hasMany(Documenttemplate::class, ['documenttype_id' => 'id']);
    }
}
