<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use fafcms\documentmanager\Bootstrap;
use fafcms\fafcms\inputs\AceEditor;
use fafcms\fafcms\inputs\DropDownList;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\OptionTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Json;

/**
 * This is the model class for table "documenttemplate".
 *
 * @property int $id
 * @property string $status
 * @property string $name
 * @property string $remarks
 * @property int $documenttype_id
 * @property int $number_current
 * @property string $number_format
 * @property int $number_step_size
 * @property string $orientation
 * @property string $css_file
 * @property string $css_content
 * @property string $css_type
 * @property string $font_files
 * @property string $default_font
 * @property double $height
 * @property double $width
 * @property double $margin_top
 * @property double $margin_right
 * @property double $margin_bottom
 * @property double $margin_left
 * @property string $data
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Document[] $documents
 * @property Documenttype $documenttype
 */
class Documenttemplate extends ActiveRecord implements FieldConfigInterface, EditViewInterface, IndexViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;

    public static string $searchModelClass = LayoutSearch::class;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/documenttemplate';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'file-hidden';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-documentmanager', 'Document templates');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-documentmanager', 'Document template');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            self::class,
            array_merge([
                self::tableName().'.id',
                self::tableName().'.name'
            ], $select ?? []),
            'id',
            static function ($item) {
                return self::extendedLabel($item);
            },
            null,
            $sort ?? [self::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => TextInput::class,
                'options' => [
                    'disabled' => true
                ]
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status']
            ],
            'remarks' => [
                'type' => AceEditor::class,
            ],
            'documenttype_id' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['documenttype_id']
            ],
            'number_current'   => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'number_format'    => $this->text()->notNull()->defaultValue('<fafcms-placeholder attribute="number" />'),
            'number_step_size' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'orientation'      => $this->string(255)->notNull()->defaultValue('portrait'),
            'css_file'         => ['type' => TextInput::class],
            'css_content'      => ['type' => TextInput::class],
            'css_type'         => ['type' => TextInput::class],
            'font_files'       => ['type' => TextInput::class],
            'default_font'     => ['type' => TextInput::class],
            'height'           => $this->float()->unsigned()->notNull()->defaultValue(29.7),
            'width'            => $this->float()->unsigned()->notNull()->defaultValue(21),
            'margin_top'       => $this->float()->unsigned()->notNull()->defaultValue(4.5),
            'margin_right'     => $this->float()->unsigned()->notNull()->defaultValue(2),
            'margin_bottom'    => $this->float()->unsigned()->notNull()->defaultValue(2.5),
            'margin_left'      => $this->float()->unsigned()->notNull()->defaultValue(2.5),
            'data'             => $this->text()->null()->defaultValue(null),
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                [
                    'class' => \fafcms\fafcms\items\Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'file-eye-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'contents' => [
                                        [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Content'],
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'content',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'link' => true,
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => \fafcms\fafcms\items\ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    /**
     * @inheritdoc
     */
    public static function prefixableTableName(): string
    {
        return '{{%documenttemplate}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['name', 'documenttype_id'], 'required'],
            [['remarks', 'number_format', 'font_files', 'data'], 'string'],
            [['documenttype_id', 'number_current', 'number_step_size', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['height', 'width', 'margin_top', 'margin_right', 'margin_bottom', 'margin_left'], 'number'],
            [['created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['status', 'name', 'orientation', 'css_file', 'css_content', 'css_type'], 'string', 'max' => 255],
            [['default_font'], 'string', 'max' => 100],
            [['documenttype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documenttype::class, 'targetAttribute' => ['documenttype_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(
            parent::attributeLabels(), [
            'id'               => Yii::t('fafcms-documentmanager', 'ID'),
            'status'           => Yii::t('fafcms-documentmanager', 'Status'),
            'name'             => Yii::t('fafcms-documentmanager', 'Name'),
            'remarks'          => Yii::t('fafcms-documentmanager', 'Remarks'),
            'documenttype_id'  => Yii::t('fafcms-documentmanager', 'Documenttype ID'),
            'number_current'   => Yii::t('fafcms-documentmanager', 'Number Current'),
            'number_format'    => Yii::t('fafcms-documentmanager', 'Number Format'),
            'number_step_size' => Yii::t('fafcms-documentmanager', 'Number Step Size'),
            'orientation'      => Yii::t('fafcms-documentmanager', 'Orientation'),
            'css_file'         => Yii::t('fafcms-documentmanager', 'Css File'),
            'css_content'      => Yii::t('fafcms-documentmanager', 'Css Content'),
            'css_type'         => Yii::t('fafcms-documentmanager', 'Css Type'),
            'font_files'       => Yii::t('fafcms-documentmanager', 'Font Files'),
            'default_font'     => Yii::t('fafcms-documentmanager', 'Default Font'),
            'height'           => Yii::t('fafcms-documentmanager', 'Height'),
            'width'            => Yii::t('fafcms-documentmanager', 'Width'),
            'margin_top'       => Yii::t('fafcms-documentmanager', 'Margin Top'),
            'margin_right'     => Yii::t('fafcms-documentmanager', 'Margin Right'),
            'margin_bottom'    => Yii::t('fafcms-documentmanager', 'Margin Bottom'),
            'margin_left'      => Yii::t('fafcms-documentmanager', 'Margin Left'),
            'data'             => Yii::t('fafcms-documentmanager', 'Data'),
            'created_by'       => Yii::t('fafcms-documentmanager', 'Created By'),
            'updated_by'       => Yii::t('fafcms-documentmanager', 'Updated By'),
            'activated_by'     => Yii::t('fafcms-documentmanager', 'Activated By'),
            'deactivated_by'   => Yii::t('fafcms-documentmanager', 'Deactivated By'),
            'deleted_by'       => Yii::t('fafcms-documentmanager', 'Deleted By'),
            'created_at'       => Yii::t('fafcms-documentmanager', 'Created At'),
            'updated_at'       => Yii::t('fafcms-documentmanager', 'Updated At'),
            'activated_at'     => Yii::t('fafcms-documentmanager', 'Activated At'),
            'deactivated_at'   => Yii::t('fafcms-documentmanager', 'Deactivated At'),
            'deleted_at'       => Yii::t('fafcms-documentmanager', 'Deleted At'),
        ]);
    }

    public function getFontFiles(): array
    {
        if (!empty($this->font_files)) {
            $files = Json::decode($this->font_files);
        }

        return $files ?? [];
    }

    /**
     * @return ActiveQuery
     */
    public function getDocumenttype(): ActiveQuery
    {
        return $this->hasOne(Documenttype::class, ['id' => 'documenttype_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocuments(): ActiveQuery
    {
        return $this->hasMany(Document::class, ['documenttemplate_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocumenttemplateelements(): ActiveQuery
    {
        return $this->hasMany(Documenttemplateelement::class, ['documenttemplate_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }
}
