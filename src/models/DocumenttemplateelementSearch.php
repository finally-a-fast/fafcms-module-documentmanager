<?php
/**
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * DocumenttemplateelementSearch represents the model behind the search form of `Documenttemplateelement`.
 */
class DocumenttemplateelementSearch extends Documenttemplateelement
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'documenttemplate_id', 'sort', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'name', 'remarks', 'content', 'type', 'float', 'align', 'valign', 'position', 'orientation_x', 'orientation_y', 'settings', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['height', 'width', 'x', 'y', 'margin_top', 'margin_right', 'margin_bottom', 'margin_left'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Documenttemplateelement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                  => $this->id,
            'documenttemplate_id' => $this->documenttemplate_id,
            'sort'                => $this->sort,
            'height'              => $this->height,
            'width'               => $this->width,
            'x'                   => $this->x,
            'y'                   => $this->y,
            'margin_top'          => $this->margin_top,
            'margin_right'        => $this->margin_right,
            'margin_bottom'       => $this->margin_bottom,
            'margin_left'         => $this->margin_left,
            'created_by'          => $this->created_by,
            'updated_by'          => $this->updated_by,
            'activated_by'        => $this->activated_by,
            'deactivated_by'      => $this->deactivated_by,
            'deleted_by'          => $this->deleted_by,
            'created_at'          => $this->created_at,
            'updated_at'          => $this->updated_at,
            'activated_at'        => $this->activated_at,
            'deactivated_at'      => $this->deactivated_at,
            'deleted_at'          => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'float', $this->float])
            ->andFilterWhere(['like', 'align', $this->align])
            ->andFilterWhere(['like', 'valign', $this->valign])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'orientation_x', $this->orientation_x])
            ->andFilterWhere(['like', 'orientation_y', $this->orientation_y])
            ->andFilterWhere(['like', 'settings', $this->settings]);

        return $dataProvider;
    }
}
