<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\models;

use fafcms\filemanager\models\File;
use fafcms\helpers\ActiveRecord;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "documentversion".
 *
 * @property int $id
 * @property int $document_id
 * @property int $version
 * @property int $file_id
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property int $deleted_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Document[] $documents
 * @property File $file
 * @property Document $document
 */
class Documentversion extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%documentversion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function save($runValidation = true, $attributeNames = null, $updateDocument = true): bool
    {
        $save = parent::save($runValidation, $attributeNames);

        if ($updateDocument) {
            $this->document->active_version            = $this->version;
            $this->document->active_documentversion_id = $this->id;
            $this->document->active_file_id            = $this->file_id;

            if (!$this->document->save()) {
                $this->addError('document', $this->document->getErrors());
                return false;
            }
        }

        return $save;
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['document_id', 'file_id'], 'required'],
            [['document_id', 'version', 'file_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['document_id' => 'id']],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['file_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id'             => Yii::t('fafcms-documentmanager', 'ID'),
            'status'         => Yii::t('fafcms-documentmanager', 'Status'),
            'document_id'    => Yii::t('fafcms-documentmanager', 'Document ID'),
            'version'        => Yii::t('fafcms-documentmanager', 'Version'),
            'file_id'        => Yii::t('fafcms-documentmanager', 'File ID'),
            'created_by'     => Yii::t('fafcms-documentmanager', 'Created By'),
            'updated_by'     => Yii::t('fafcms-documentmanager', 'Updated By'),
            'activated_by'   => Yii::t('fafcms-documentmanager', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-documentmanager', 'Deactivated By'),
            'deleted_by'     => Yii::t('fafcms-documentmanager', 'Deleted By'),
            'created_at'     => Yii::t('fafcms-documentmanager', 'Created At'),
            'updated_at'     => Yii::t('fafcms-documentmanager', 'Updated At'),
            'activated_at'   => Yii::t('fafcms-documentmanager', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-documentmanager', 'Deactivated At'),
            'deleted_at'     => Yii::t('fafcms-documentmanager', 'Deleted At'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getFile(): ActiveQuery
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocument(): ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'document_id']);
    }
}
