<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\controllers;

use fafcms\documentmanager\models\Documenttype;
use fafcms\helpers\DefaultController;

/**
 * Class DocumenttypeController
 *
 * @package fafcms\documentmanager\controllers
 */
class DocumenttypeController extends DefaultController
{
    /** @var string  */
    public static $modelClass = Documenttype::class;
}
