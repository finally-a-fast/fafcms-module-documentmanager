<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager\controllers;

use Exception;
use fafcms\filemanager\models\Filegroup;
use fafcms\documentmanager\{
    models\Document,
    models\Documenttemplate,
    renderer\PdfRenderer,
};
use fafcms\parser\component\Parser;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Yii;
use yii\helpers\{
    Inflector,
    Json,
};
use yii\web\{
    NotFoundHttpException,
    Controller,
};
use yii\web\Response;

/**
 * Class DocumentmanagerController
 *
 * @package fafcms\documentmanager\controllers
 */
class DocumentmanagerController extends Controller
{
    /**
     * @param integer|null $id
     *
     * @return Response
     * @throws Exception|Html2PdfException|NotFoundHttpException
     */
    public function actionPreview(?int $id = null): Response
    {
        $model = $this->findModel($id);
        $data  = Json::decode($model->data, true);

        if ($data === null) {
            $data = [];
        }

        $meta     = self::getMetaData($data);
        $fileName = Inflector::slug(Yii::$app->fafcmsParser->parse(Parser::TYPE_PDF, $model->name, Parser::ROOT, $meta));

        if (($pdf = PdfRenderer::renderDocument($model->id, $meta)) === false) {
            Yii::$app->getSession()->setFlash('error', Yii::t('fafcms-documentmanager', 'There was a problem while generating the preview.'));
            return $this->goBack(); // todo: redirect to site with 'generate preview' button
        }

        $pdf->pdf->SetTitle($fileName);
        $output = $pdf->output($fileName . '.pdf', 'S');

        return Yii::$app->response->sendContentAsFile($output, $fileName . '.pdf', [
            'mimeType' => 'application/pdf',
            'inline'   => true,
        ]);
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDocument(string $hashId): Response
    {
        if (($document = Document::find()->where(['hashId' => $hashId])->one()) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return Yii::$app->runAction('fafcms-filemanager/filemanager/get-file', ['id' => $document->active_file_id]);
    }

    /***
     * @param array $datas
     * @return array
     */
    public static function getMetaData(array $datas): array
    {
        $meta = [];
        $preview = Yii::$app->getRequest()->get('preview');

        foreach ($datas as $name => $metaModel) {
            if (is_string($metaModel)) {
                $className = $metaModel;
                $dataModel = $className::find();
            } else {
                $className = $metaModel['class'];
                $dataModel = $className::find();
            }

            $meta[$name] = $dataModel->filterWhere(['id' => $preview[$name] ?? null])->one();

            if (!is_string($metaModel) && isset($metaModel['relations'])) {
                foreach ($metaModel['relations'] as $relation) {
                    $metaName = $name . '.' . $relation;
                    $getter = 'get' . Inflector::pluralize(ucfirst($relation));

                    try {
                        $metaRelation = $meta[$name]->$getter();
                    } catch (Exception $e) {
                        continue;
                    }

                    $relationFilterId = $preview[$metaName]??null;
                    $relationClass = $metaRelation->modelClass;

                    if (!empty($relationFilterId)) {
                        $metaRelation = $relationClass::find(true)->from([$metaRelation])->where([($relationClass::$optionId ?? 'id') => $relationFilterId]);
                    }

                    $meta[$metaName] = $metaRelation->one();
                }
            }
        }

        return $meta;
    }

    /**
     * Finds the Documenttemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer|null $id
     *
     * @return Documenttemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(?int $id): Documenttemplate
    {
        if (($model = Documenttemplate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
