<?php

namespace fafcms\documentmanager\migrations;

use fafcms\documentmanager\models\Documenttemplate;
use fafcms\documentmanager\models\Documenttemplatetranslation;
use yii\db\Migration;

/**
 * Class m200820_114830_css
 *
 * @package fafcms\documentmanager\migrations
 */
class m200820_114830_css extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Documenttemplate::tableName(), 'css_file', $this->string(255)->null()->defaultValue(null)->after('orientation'));
        $this->addColumn(Documenttemplate::tableName(), 'css_content', $this->string(255)->null()->defaultValue(null)->after('css_file'));
        $this->addColumn(Documenttemplate::tableName(), 'css_type', $this->string(255)->notNull()->defaultValue('scss')->after('css_content'));
    }

    public function safeDown()
    {
        $this->dropColumn(Documenttemplate::tableName(), 'css_file');
        $this->dropColumn(Documenttemplate::tableName(), 'css_content');
        $this->dropColumn(Documenttemplate::tableName(), 'css_type');
    }
}
