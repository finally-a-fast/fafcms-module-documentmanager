<?php

namespace fafcms\documentmanager\migrations;

use fafcms\documentmanager\models\Document;
use fafcms\documentmanager\models\Documenttemplate;
use fafcms\documentmanager\models\Documenttemplateelement;
use fafcms\documentmanager\models\Documenttype;
use fafcms\documentmanager\models\Documentversion;
use fafcms\filemanager\models\File;
use yii\db\Migration;

/**
 * Class m200819_095441_init
 *
 * @package fafcms\documentmanager\migrations
 */
class m200819_095441_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Documenttype::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'remarks' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-documenttype-created_by', Documenttype::tableName(), ['created_by'], false);
        $this->createIndex('idx-documenttype-updated_by', Documenttype::tableName(), ['updated_by'], false);
        $this->createIndex('idx-documenttype-activated_by', Documenttype::tableName(), ['activated_by'], false);
        $this->createIndex('idx-documenttype-deactivated_by', Documenttype::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-documenttype-deleted_by', Documenttype::tableName(), ['deleted_by'], false);

        $this->createTable(Documenttemplate::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'remarks' => $this->text()->null()->defaultValue(null),
            'documenttype_id' => $this->integer(10)->unsigned()->notNull(),
            'number_current' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'number_format' => $this->text()->notNull()->defaultValue('<fafcms-placeholder attribute="number" />'),
            'number_step_size' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'orientation' => $this->string(255)->notNull()->defaultValue('portrait'),
            'height' => $this->float()->unsigned()->notNull()->defaultValue(29.7),
            'width' => $this->float()->unsigned()->notNull()->defaultValue(21),
            'margin_top' => $this->float()->unsigned()->notNull()->defaultValue(4.5),
            'margin_right' => $this->float()->unsigned()->notNull()->defaultValue(2),
            'margin_bottom' => $this->float()->unsigned()->notNull()->defaultValue(2.5),
            'margin_left' => $this->float()->unsigned()->notNull()->defaultValue(2.5),
            'data' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-documenttemplate-documenttype_id', Documenttemplate::tableName(), ['documenttype_id'], false);
        $this->createIndex('idx-documenttemplate-created_by', Documenttemplate::tableName(), ['created_by'], false);
        $this->createIndex('idx-documenttemplate-updated_by', Documenttemplate::tableName(), ['updated_by'], false);
        $this->createIndex('idx-documenttemplate-activated_by', Documenttemplate::tableName(), ['activated_by'], false);
        $this->createIndex('idx-documenttemplate-deactivated_by', Documenttemplate::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-documenttemplate-deleted_by', Documenttemplate::tableName(), ['deleted_by'], false);

        $this->createTable(Documenttemplateelement::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'remarks' => $this->text()->null()->defaultValue(null),
            'documenttemplate_id' => $this->integer(10)->unsigned()->notNull(),
            'content' => $this->text()->null()->defaultValue(null),
            'type' => $this->string(255)->notNull()->defaultValue('content'),
            'sort' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'height' => $this->float()->unsigned()->null()->defaultValue(null),
            'width' => $this->float()->unsigned()->null()->defaultValue(null),
            'float' => $this->string(255)->notNull()->defaultValue('left'),
            'align' => $this->string(255)->notNull()->defaultValue('left'),
            'valign' => $this->string(255)->notNull()->defaultValue('top'),
            'position' => $this->string(255)->notNull()->defaultValue('static'),
            'x' => $this->float()->null()->defaultValue(null),
            'y' => $this->float()->null()->defaultValue(null),
            'orientation_x' => $this->string(255)->notNull()->defaultValue('left'),
            'orientation_y' => $this->string(255)->notNull()->defaultValue('top'),
            'margin_top' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'margin_right' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'margin_bottom' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'margin_left' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'settings' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-documenttemplateelement-documenttemplate_id', Documenttemplateelement::tableName(), ['documenttemplate_id'], false);
        $this->createIndex('idx-documenttemplateelement-created_by', Documenttemplateelement::tableName(), ['created_by'], false);
        $this->createIndex('idx-documenttemplateelement-updated_by', Documenttemplateelement::tableName(), ['updated_by'], false);
        $this->createIndex('idx-documenttemplateelement-activated_by', Documenttemplateelement::tableName(), ['activated_by'], false);
        $this->createIndex('idx-documenttemplateelement-deactivated_by', Documenttemplateelement::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-documenttemplateelement-deleted_by', Documenttemplateelement::tableName(), ['deleted_by'], false);

        $this->createTable(Documentversion::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'document_id' => $this->integer(10)->unsigned()->notNull(),
            'version' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'file_id' => $this->integer(10)->unsigned()->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-documentversion-document_id', Documentversion::tableName(), ['document_id'], false);
        $this->createIndex('idx-documentversion-file_id', Documentversion::tableName(), ['file_id'], false);
        $this->createIndex('idx-documentversion-created_by', Documentversion::tableName(), ['created_by'], false);
        $this->createIndex('idx-documentversion-updated_by', Documentversion::tableName(), ['updated_by'], false);
        $this->createIndex('idx-documentversion-activated_by', Documentversion::tableName(), ['activated_by'], false);
        $this->createIndex('idx-documentversion-deactivated_by', Documentversion::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-documentversion-deleted_by', Documentversion::tableName(), ['deleted_by'], false);

        $this->createTable(Document::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'documenttemplate_id' => $this->integer(10)->unsigned()->notNull(),
            'number' => $this->string(255)->notNull(),
            'current_version' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'current_documentversion_id' => $this->integer(10)->unsigned()->notNull(),
            'current_file_id' => $this->integer(10)->unsigned()->notNull(),
            'model_class' => $this->string(255)->null()->defaultValue(null),
            'model_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-document-documenttemplate_id', Document::tableName(), ['documenttemplate_id'], false);
        $this->createIndex('idx-document-current_documentversion_id', Document::tableName(), ['current_documentversion_id'], false);
        $this->createIndex('idx-document-current_file_id', Document::tableName(), ['current_file_id'], false);
        $this->createIndex('idx-document-created_by', Document::tableName(), ['created_by'], false);
        $this->createIndex('idx-document-updated_by', Document::tableName(), ['updated_by'], false);
        $this->createIndex('idx-document-activated_by', Document::tableName(), ['activated_by'], false);
        $this->createIndex('idx-document-deactivated_by', Document::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-document-deleted_by', Document::tableName(), ['deleted_by'], false);

        $this->addForeignKey('fk-documenttemplate-documenttype_id', Documenttemplate::tableName(), 'documenttype_id', Documenttype::tableName(), 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-documenttemplateelement-documenttemplate_id', Documenttemplateelement::tableName(), 'documenttemplate_id', Documenttemplate::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk-documentversion-document_id', Documentversion::tableName(), 'document_id', Document::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-documentversion-file_id', Documentversion::tableName(), 'file_id', File::tableName(), 'id', 'RESTRICT', 'CASCADE');

        $this->addForeignKey('fk-document-documenttemplate_id', Document::tableName(), 'documenttemplate_id', Documenttemplate::tableName(), 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-document-current_documentversion_id', Document::tableName(), 'current_documentversion_id', Documentversion::tableName(), 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-document-current_file_id', Document::tableName(), 'current_file_id', File::tableName(), 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-documenttemplate-documenttype_id', Documenttemplate::tableName());
        $this->dropForeignKey('fk-documenttemplateelement-documenttemplate_id', Documenttemplateelement::tableName());

        $this->dropForeignKey('fk-documentversion-document_id', Documentversion::tableName());
        $this->dropForeignKey('fk-documentversion-file_id', Documentversion::tableName());

        $this->dropForeignKey('fk-document-documenttemplate_id', Document::tableName());
        $this->dropForeignKey('fk-document-current_documentversion_id', Document::tableName());
        $this->dropForeignKey('fk-document-current_file_id', Document::tableName());

        $this->dropTable(Documenttype::tableName());
        $this->dropTable(Documenttemplate::tableName());
        $this->dropTable(Documenttemplateelement::tableName());
        $this->dropTable(Documentversion::tableName());
        $this->dropTable(Document::tableName());
    }
}
