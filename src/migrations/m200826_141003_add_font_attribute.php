<?php

namespace fafcms\documentmanager\migrations;

use fafcms\documentmanager\models\Documenttemplate;
use yii\db\Migration;

/**
 * Class m200826_141003_add_font_attribute
 */
class m200826_141003_add_font_attribute extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Documenttemplate::tableName(), 'font_files', $this->text()->null()->after('css_type'));
        $this->addColumn(Documenttemplate::tableName(), 'default_font', $this->string(100)->null()->after('font_files'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Documenttemplate::tableName(), 'font_files');
        $this->dropColumn(Documenttemplate::tableName(), 'default_font');
    }
}
