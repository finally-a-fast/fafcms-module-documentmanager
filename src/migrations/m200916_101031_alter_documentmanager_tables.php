<?php

namespace fafcms\documentmanager\migrations;

use fafcms\documentmanager\models\Document;
use fafcms\documentmanager\models\Documenttemplate;
use fafcms\documentmanager\models\Documentversion;
use fafcms\filemanager\models\File;
use yii\db\Migration;

/**
 * Class m200916_101031_alter_documentmanager_tables
 */
class m200916_101031_alter_documentmanager_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-document-current_documentversion_id', Document::tableName());
        $this->dropForeignKey('fk-document-current_file_id', Document::tableName());
        $this->dropForeignKey('fk-documentversion-file_id', Documentversion::tableName());

        $this->dropIndex('idx-document-current_documentversion_id', Document::tableName());
        $this->dropIndex('idx-document-current_file_id', Document::tableName());

        $this->alterColumn(Documenttemplate::tableName(), 'name', $this->text()->notNull());

        $this->alterColumn(Document::tableName(), 'number', $this->string(255)->null());
        $this->alterColumn(Document::tableName(), 'current_documentversion_id', $this->integer(10)->unsigned()->null());
        $this->alterColumn(Document::tableName(), 'current_file_id', $this->integer(10)->unsigned()->null());

        $this->renameColumn(Document::tableName(), 'current_version', 'active_version');
        $this->renameColumn(Document::tableName(), 'current_documentversion_id', 'active_documentversion_id');
        $this->renameColumn(Document::tableName(), 'current_file_id', 'active_file_id');

        $this->createIndex('idx-document-active_documentversion_id', Document::tableName(), ['active_documentversion_id'], false);
        $this->createIndex('idx-document-active_file_id', Document::tableName(), ['active_file_id'], false);

        $this->addForeignKey('fk-document-active_documentversion_id', Document::tableName(), 'active_documentversion_id', Documentversion::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-document-active_file_id', Document::tableName(), 'active_file_id', File::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-documentversion-file_id', Documentversion::tableName(), 'file_id', File::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-document-active_documentversion_id', Document::tableName());
        $this->dropForeignKey('fk-document-active_file_id', Document::tableName());
        $this->dropForeignKey('fk-documentversion-file_id', Documentversion::tableName());

        $this->dropIndex('idx-document-active_documentversion_id', Document::tableName());
        $this->dropIndex('idx-document-active_file_id', Document::tableName());

        $this->alterColumn(Documenttemplate::tableName(), 'name', $this->string(255)->notNull());

        $this->renameColumn(Document::tableName(),'active_version', 'current_version');
        $this->renameColumn(Document::tableName(),'active_documentversion_id', 'current_documentversion_id');
        $this->renameColumn(Document::tableName(),'active_file_id', 'current_file_id');

        $this->alterColumn(Document::tableName(), 'number', $this->string(255)->notNull());
        $this->alterColumn(Document::tableName(), 'current_documentversion_id', $this->integer(10)->unsigned()->notNull());
        $this->alterColumn(Document::tableName(), 'current_file_id', $this->integer(10)->unsigned()->notNull());

        $this->createIndex('idx-document-current_documentversion_id', Document::tableName(), ['current_documentversion_id'], false);
        $this->createIndex('idx-document-current_file_id', Document::tableName(), ['current_file_id'], false);

        $this->addForeignKey('fk-document-current_documentversion_id', Document::tableName(), 'current_documentversion_id', Documentversion::tableName(), 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-document-current_file_id', Document::tableName(), 'current_file_id', File::tableName(), 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-documentversion-file_id', Documentversion::tableName(), 'file_id', File::tableName(), 'id', 'RESTRICT', 'CASCADE');
    }
}
