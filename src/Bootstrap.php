<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-documentmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-documentmanager/docs Documentation of fafcms-module-documentmanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\documentmanager;

use fafcms\documentmanager\models\{
    Document,
    Documenttemplate,
    Documenttype,
};
use fafcms\fafcms\components\FafcmsComponent;
use Yii;
use yii\base\Application;
use fafcms\helpers\abstractions\{
    PluginBootstrap,
    PluginModule,
};
use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 *
 * @package fafcms\documentmanager
 */
class Bootstrap extends PluginBootstrap
{
    /** @var string  */
    public static $id = 'fafcms-documentmanager';

    /** @var string  */
    public static $tablePrefix = 'fafcms-document_';

    /**
     * {@inheritdoc}
     */
    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations[self::$id])) {
            $app->i18n->translations[self::$id] = [
                'class'            => PhpMessageSource::class,
                'basePath'         => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'project' => [
                    'items' => [
                        'documents'    => [
                            'after' => 'files',
                            'icon' => 'file-document-multiple-outline',
                            'label' => Yii::t('fafcms-documentmanager', 'Document Management'),
                            'items' => [
                                'documenttypes' => [
                                    'icon'  => Documenttype::instance()->getEditData()['icon'],
                                    'label' => Documenttype::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Documenttype::instance()->getEditData()['url'] . '/index'],
                                ],
                                'documenttemplates' => [
                                    'icon'  => Documenttemplate::instance()->getEditData()['icon'],
                                    'label' => Documenttemplate::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Documenttemplate::instance()->getEditData()['url'] . '/index'],
                                ],
                                'documents' => [
                                    'icon'  => Document::instance()->getEditData()['icon'],
                                    'label' => Document::instance()->getEditData()['plural'],
                                    'url'   => ['/' . Document::instance()->getEditData()['url'] . '/index'],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>'                     => '<controller>/<action>',
        ]);

        $app->fafcms->addFrontendUrlRules(self::$id, [
            'assets/document/<hashId:[a-zA-Z0-9\-]+>' => 'documentmanager/document',
        ], false);

        return true;
    }
}
